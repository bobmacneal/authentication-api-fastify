const boom = require('@hapi/boom')
const { DateTime } = require('luxon')
const User = require('../models/User')
const constants = require('../constants')

exports.getUsers = async () => {
  try {
    return await User.find()
  } catch (err) {
    throw boom.boomify(err)
  }
}

exports.getUser = async (req, reply) => {
  try {
    const user = await User.findOne({ _id: req.params.id })
    return user || reply.status(constants.ResponseStatus.NOT_FOUND).send({ error: 'User not found!' })
  } catch (err) {
    throw boom.boomify(err)
  }
}

exports.updateUser = async (req) => {
  try {
    const id = req.params.id
    const user = req.body
    user.lastUpdatedAt = DateTime.utc().valueOf()
    const { ...updateData } = user
    return await User.findByIdAndUpdate(id, updateData, { new: true, useFindAndModify: false })
  } catch (err) {
    throw boom.boomify(err)
  }
}

exports.deleteUser = async (req) => {
  try {
    const id = req.params.id
    return await User.findByIdAndDelete(id, { useFindAndModify: false })
  } catch (err) {
    throw boom.boomify(err)
  }
}
