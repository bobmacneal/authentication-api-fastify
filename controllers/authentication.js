const bcrypt = require('bcryptjs')
const constants = require('../constants')
const User = require('../models/User')

exports.register = async (request, reply) => {
  if (!(request.body.email && request.body.password && request.body.username)) {
    return reply.status(constants.ResponseStatus.BAD_REQUEST).send({ error: 'Required field missing!' })
  }
  const { email, password, username } = request.body
  try {
    const existingUser = await User.findOne({ email })
    if (existingUser) {
      return reply.status(constants.ResponseStatus.CONFLICT).send({ error: `${email} already registered!` })
    }
    const user = new User({
      email: email.toLowerCase(),
      password,
      username
    })
    await user.save()
    const {accessToken, refreshToken} = await user.getUserTokens()
    const payload = {
      apiKey: process.env.API_KEY,
      email,
      id: user._id,
      username,
      accessToken,
      refreshToken
    }
    reply.status(constants.ResponseStatus.CREATED).send(payload)
  } catch (error) {
    reply.status(constants.ResponseStatus.BAD_REQUEST).send(error)
  }
}

exports.login = async (request, reply) => {
  if (!(request.body.email && request.body.password)) {
    // reply.status(constants.ResponseStatus.BAD_REQUEST).send('Missing input!')
    reply.status(constants.ResponseStatus.BAD_REQUEST).send({ error: 'Required fields missing!' })
  }
  try {
    const { email, password } = request.body
    const user = await User.findOne({ email })
    if (!user) {
      return reply.status(constants.ResponseStatus.NOT_FOUND).send({ error: 'No user found!' })
    } else {
      const valid = await bcrypt.compare(password, user.password)
      if (valid) {
        const {accessToken, refreshToken} = await user.getUserTokens()
        const payload = {
          apiKey: process.env.API_KEY,
          email,
          id: user._id,
          username: user.username,
          accessToken,
          refreshToken
        }
        reply.status(constants.ResponseStatus.OK).send(payload)
      } else {
        return reply.status(constants.ResponseStatus.UNAUTHORIZED).send({ error: 'Wrong password!' })
      }
    }
  } catch (error) {
    reply.status(constants.ResponseStatus.SERVER_ERROR).send({ error: 'Internal Server Error!' })
  }
}

exports.logout = async (request, reply) => {
  try {
    reply.status(constants.ResponseStatus.OK).send(`${request.user.username} is logged out`)
  } catch (error) {
    reply.status(constants.ResponseStatus.SERVER_ERROR).send({ error: 'Internal Server Error!' })
  }
}

exports.account = async (request, reply) => {
  const payload = {
    email: request.user.email,
    id: request.user._id,
    username: request.user.username
  }
  reply.status(constants.ResponseStatus.OK).send(payload)
}
