const ResponseStatus = {
  SERVER_ERROR: 500,
  CONFLICT: 409,
  NOT_FOUND: 404,
  FORBIDDEN: 403,
  UNAUTHORIZED: 401,
  BAD_REQUEST: 400,
  CREATED: 201,
  OK: 200
}

module.exports = {
  ResponseStatus
}
