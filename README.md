# Authentication Template API
This is an authentication template API. The API runs on a [fastify](https://www.fastify.io/) web server
against a [mongoDB](https://www.mongodb.com/) document database. 

See [Fastify cheatsheet](https://devhints.io/fastify)

# Setup

## MongoDB 

Coming soon.

To determine if mongod service is running
````
$ ps aux | grep -v grep | grep mongod
````
To start mongod service
````
$ mongod --config /usr/local/etc/mongod.conf
````  


## MongoDB Compass

Coming soon

## API Source Code

Coming soon

# Usage
| command script  | description  | notes |
|---|---|---|
| `npm i`  | Installs/refreshes node package dependencies |---|
| `npm run start`  |  Starts server.js in env | Running per .env file. |
| `npm run dev`  |  Starts server.js within _nodemon_ | Local developer mode. _nodemon_ listening for changes. |
| `npm run lint`  |  Runs standard --fix  | Standard linting. Fixes most syntax errors. |
