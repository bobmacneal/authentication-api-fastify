'use strict'

require('dotenv').config()

const Fastify = require('fastify')
const closeWithGrace = require('close-with-grace') // to exit fastify process, gracefully (if possible)

const app = Fastify({
  logger: true
})

require('mongoose')

const db = require('./config/db')
const uri = process.env.MONGO_URI

app.register(require('./routes'))
app.register(db, { uri })
app.register(require('fastify-helmet'))
app.register(require('fastify-rate-limit'), {
  max: 500,
  timeWindow: '1 minute'
  // allowList: ['127.0.0.1'], // default []
})
app.register(require('fastify-cors'))

// delay is the number of milliseconds for the graceful close to finish
const closeListeners = closeWithGrace({ delay: 500 }, async function ({ signal, err, manual }) {
  if (err) {
    app.log.error(err)
  }
  await app.close()
})

app.addHook('onClose', async (instance, done) => {
  closeListeners.uninstall()
  done()
})

const start = async () => {
  try {
    await app.listen(process.env.PORT || 5000, '0.0.0.0')
  } catch (err) {
    app.log.error(err)
    process.exit(1)
  }
}
start().then(() => app.log.info('READY'))
