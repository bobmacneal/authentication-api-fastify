const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { DateTime } = require('luxon')
const { ACCESS_TOKEN_SECRET, REFRESH_TOKEN_SECRET } = process.env

const userSchema = Schema({
  email: {
    type: String,
    minlength: 3,
    maxlength: 254,
    required: [true, 'required field'],
    trim: true,
    unique: true,
    match: [
      /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
      'Must be a valid email'
    ]
  },
  password: {
    type: String,
    required: [true, 'required field'],
    minlength: 6
  },
  username: {
    type: String,
    minlength: 1,
    maxlength: 25,
    trim: true,
    unique: false,
    required: [true, 'required field']
  },
  created: {
    default: DateTime.utc().valueOf(),
    type: Number
  }
})

userSchema.pre('save', async function (next) {
  const user = this
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8)
  }
  next()
})

userSchema.methods.getUserTokens = async function () {
  const user = this
  const signingPayload = {
    userInfo: {
      id: user._id.toString(),
      role: 'some role'
    }
  }
  const accessToken = jwt.sign(
    signingPayload,
    ACCESS_TOKEN_SECRET,
    {
      expiresIn: '1d'
    }
  )
  const refreshToken = jwt.sign(
    signingPayload,
    REFRESH_TOKEN_SECRET,
    {
      expiresIn: '5d'
    }
  )
  return {
    accessToken,
    refreshToken
  }
}

userSchema.set('toJSON', { // makes sure password not returned in payload
  transform: function (doc, response) {
    delete response.password
    return response
  }
})

const User = mongoose.model('User', userSchema)

module.exports = User
