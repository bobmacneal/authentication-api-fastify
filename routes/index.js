const fastifyAuth = require('fastify-auth')
const User = require('../models/User')
const jwt = require('jsonwebtoken')
const authController = require('../controllers/authentication')
const userController = require('../controllers/user')
const constants = require('../constants')

const setOptions = (handler, preHandler, schema, logLevel) => {
  const options = {
    handler,
    logLevel: logLevel || 'warn'
  }
  if (preHandler) {
    options.preHandler = preHandler
  }
  if (schema) {
    options.schema = schema
  }
  return options
}

const sanitizedUser = {
  type: 'object',
  properties: {
    _id: { type: 'string' },
    createdAt: { type: 'number' },
    email: { type: 'string' },
    username: { type: 'string' }
  }
}

const usersResponse = {
  response: {
    200: {
      type: 'array',
      items: sanitizedUser
    }
  }
}

const singleUserResponse = {
  response: {
    200: sanitizedUser
  }
}

const routes = async (fastify) => {
  fastify
    .decorate('verifyJwt', async (request, reply) => {
      if (!request.headers.authorization || !request.headers.authorization.includes('JWT ', 0)) {
        reply.code(constants.ResponseStatus.UNAUTHORIZED).send()
      }
      const token = request.headers.authorization.replace('JWT ', '')
      try {
        const { userInfo } = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
        const user = await User.findById(userInfo.id)
        if (user) {
          request.user = user
          request.accessToken = token
        } else {
          reply.code(constants.ResponseStatus.UNAUTHORIZED).send()
        }
      } catch (error) {
        if (error.message === 'jwt expired') {
          console.log('TODO: do something about expired jwt')
          // const refreshToken = request.headers['x-refresh-token']
          // if (refreshToken) {
          //   const { userInfo } = jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET)
          //   const user = await User.findById(userInfo.id)
          //   if (user) {
          //     request.user = user
          //     const renewed = await user.getUserTokens()
          //     if (renewed.accessToken && renewed.refreshToken) {
          //       console.error('Setting renewed tokens in reply header...')
          //       reply.setHeader('Access-Control-Expose-Headers', 'authorization, x-refresh-token')
          //       reply.setHeader('authorization', renewed.accessToken)
          //       reply.setHeader('x-refresh-token', renewed.refreshToken)
          //       console.error('Setting renewed tokens in reply header SEEMED TO WORK!')
          //     }
          //   } else {
          //     console.error('Could not resolve user from refresh token!')
          //     reply.code(constants.ResponseStatus.UNAUTHORIZED).send()
          //   }
          // } else {
          //   console.error('No refresh token!')
          //   reply.code(constants.ResponseStatus.UNAUTHORIZED).send()
          // }
        } else if (error.message === 'jwt malformed') {
          console.error('malformed jwt!')
        } else {
          console.error(`====ERROR verifying token => ${error.message}`)
        }
      }
    })
    .register(fastifyAuth)
    .after(() => {
      // accounts
      fastify.get('/v1/account', setOptions(authController.account, fastify.auth([fastify.verifyJwt])))
      fastify.post('/v1/register', setOptions(authController.register))
      fastify.post('/v1/login', setOptions(authController.login))
      fastify.get('/v1/logout', setOptions(authController.logout, fastify.auth([fastify.verifyJwt])))

      // users
      fastify.get('/v1/users', setOptions(userController.getUsers, fastify.auth([fastify.verifyJwt]), usersResponse))
      fastify.get('/v1/users/:id', setOptions(userController.getUser, fastify.auth([fastify.verifyJwt]), singleUserResponse))
      fastify.put('/v1/users/:id', setOptions(userController.updateUser, fastify.auth([fastify.verifyJwt])))
      fastify.delete('/v1/users/:id', setOptions(userController.deleteUser, fastify.auth([fastify.verifyJwt])))
    })
}

module.exports = routes
